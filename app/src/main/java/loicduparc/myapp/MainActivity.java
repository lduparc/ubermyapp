package loicduparc.myapp;

import android.app.SearchManager;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import loicduparc.myapp.models.ImagesEntity;
import loicduparc.myapp.models.SearchEvent;
import loicduparc.myapp.provider.SearchSuggestionProvider;


public class MainActivity extends ActionBarActivity implements SearchView.OnQueryTextListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    protected final String ARG_CACHED_DATA = TAG + ":data";

    @InjectView(R.id.toolbar)
    Toolbar mToolbar;
    @InjectView(R.id.searchHistory)
    ListView mSearchHistory;

    private MainFragment mFragment;
    private SearchView mSearchView;

    private ArrayList<ImagesEntity> mCachedData;

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * LifeCycle
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.inject(this);

        this.mToolbar.setLogo(R.mipmap.ic_launcher);
        setSupportActionBar(this.mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (savedInstanceState != null) {
            this.mCachedData = savedInstanceState.getParcelableArrayList(ARG_CACHED_DATA);
        } else {
            this.mCachedData = new ArrayList<>();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        this.mFragment = (MainFragment) getSupportFragmentManager().findFragmentByTag(TAG);
        if (this.mFragment == null) {
            this.mFragment = MainFragment.newInstance(mCachedData);
            fragmentTransaction.add(R.id.container, this.mFragment, TAG).commit();
        } else {
            fragmentTransaction.attach(this.mFragment).commit();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (this.mFragment != null) {
            getSupportFragmentManager().beginTransaction().detach(mFragment).commit();
        }

        //Just for the example...
        SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this, SearchSuggestionProvider.AUTHORITY, SearchSuggestionProvider.MODE);
        suggestions.clearHistory();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(ARG_CACHED_DATA, mCachedData != null ? this.mCachedData : new ArrayList<ImagesEntity>());
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Menu
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        this.mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        this.mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        this.mSearchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuId = item.getItemId();
        if (menuId == R.id.action_search) {
            this.mSearchView.setIconified(false);
            return true;
        }
        return false;
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * SearchView.OnQueryTextListener
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    @Override
    public boolean onQueryTextSubmit(String constraint) {
        if (!TextUtils.isEmpty(constraint)) {
            EventBus.getDefault().post(new SearchEvent(constraint, true));

            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this, SearchSuggestionProvider.AUTHORITY, SearchSuggestionProvider.MODE);
            suggestions.saveRecentQuery(constraint, null);
        }

        hideKeyboard();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String constraint) {

        if (!TextUtils.isEmpty(constraint)) {
            ContentResolver contentResolver = getApplicationContext().getContentResolver();

            String contentUri = "content://" + SearchSuggestionProvider.AUTHORITY + '/' + SearchManager.SUGGEST_URI_PATH_QUERY;
            Uri uri = Uri.parse(contentUri);

            final Cursor cursor = contentResolver.query(uri, null, null, new String[]{constraint}, null);
            cursor.moveToFirst();

            final String[] columns = new String[]{SearchManager.SUGGEST_COLUMN_TEXT_1};
            int[] views = new int[]{R.id.name};

            ListAdapter listAdapter = new SimpleCursorAdapter(this, R.layout.layout_search_history_item, cursor, columns, views, 0);
            mSearchHistory.setAdapter(listAdapter);
            mSearchHistory.setVisibility(View.VISIBLE);
            mSearchHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    mSearchView.setQuery(cursor.getString(cursor.getColumnIndex(SearchManager.SUGGEST_COLUMN_TEXT_1)), true);
                    mSearchHistory.setVisibility(View.GONE);
                }
            });
        } else {
            mSearchHistory.setAdapter(null);
            mSearchHistory.setVisibility(View.GONE);
        }
        return true;
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Utils
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    private void hideKeyboard() {
        if (this.mSearchView != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(this.mSearchView.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
        }
    }
}
