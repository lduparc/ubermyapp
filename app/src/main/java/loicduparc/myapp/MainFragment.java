package loicduparc.myapp;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import loicduparc.myapp.communication.RetrofitController;
import loicduparc.myapp.models.ImagesEntity;
import loicduparc.myapp.models.ResponseData;
import loicduparc.myapp.models.SearchEvent;
import loicduparc.myapp.utils.CustomDialog;
import loicduparc.myapp.utils.EndlessRecyclerOnScrollListener;
import loicduparc.myapp.utils.ItemClickEvent;
import loicduparc.myapp.utils.LoadMoreEvent;
import loicduparc.myapp.utils.UiHelper;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainFragment extends Fragment {

    private static final String TAG = MainFragment.class.getSimpleName();
    private static final String ARG_QUERY = TAG + ":query";
    private static final String ARG_PAGE_IDX = TAG + ":pageIdx";
    private static final String ARG_DATA = TAG + ":data";
    private static final int MIN_ITEM_COUNT = 10;

    @InjectView(R.id.recycler)
    RecyclerView mRecyclerView;

    private ArrayList<ImagesEntity> mDataSet;
    private int mCurrentPage;
    private String mCurrentQuery;
    private CustomDialog mCustomDialog;

    public MainFragment() {
    }

    public static MainFragment newInstance(ArrayList<ImagesEntity> dataSet) {
        MainFragment fragment = new MainFragment();

        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_DATA, dataSet != null ? dataSet : new ArrayList<ImagesEntity>());
        fragment.setArguments(args);

        return fragment;
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * LifeCycle
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDataSet = getArguments().getParcelableArrayList(ARG_DATA);
        mCurrentPage = getArguments().getInt(ARG_PAGE_IDX, 0);
        mCurrentQuery = getArguments().getString(ARG_QUERY, "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final Context context = getActivity();

        LinearLayoutManager layoutManager = createLayoutManager(context);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(new ImageViewAdapter(context, mDataSet));
        mRecyclerView.setOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                onEvent(new SearchEvent(MainFragment.this.mCurrentQuery, false, MainFragment.this.mCurrentPage + 1));
            }
        });
    }

    @Override
    public void onResume() {
        EventBus.getDefault().register(this);
        super.onResume();
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * LayoutManager Helper
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    private LinearLayoutManager createLayoutManager(Context context) {
        boolean isLarge = UiHelper.isTablet(context);
        boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
        int span = 1;
        if (isLarge) {
            span = isPortrait ? 2 : 4;
        } else {
            span = isPortrait ? 1 : 2;
        }

        return new GridLayoutManager(context, span);
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Event
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    public void onEvent(final SearchEvent event) {
        this.mCurrentQuery = event.getConstraint();

        //This is not a bug, I force this.mCurrentPage +1 until the data list is bigger than the screen size and on each scroll end.
        this.mCurrentPage = event.isNewQuery() || mDataSet.size() < MIN_ITEM_COUNT ? 0 : event.getPage();

        RetrofitController.getInstance().search(event, new Callback<ResponseData>() {
            @Override
            public void success(ResponseData data, Response response) {
                if (event.isNewQuery()) {
                    mDataSet.clear();
                }
                mDataSet.addAll(data.getResponseData().getResults());
                mRecyclerView.getAdapter().notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }


    public void onEvent(LoadMoreEvent event) {
        onEvent(new SearchEvent(this.mCurrentQuery, false, this.mCurrentPage + 1));
    }

    public void onEvent(ItemClickEvent event) {
        final Activity activity = getActivity();
        if (activity != null && !activity.isFinishing()) {
            if (UiHelper.isTablet(activity)) {
                showDialog(activity, event);
            } else {
                PictureDetailActivity.launch(activity, event);
            }
        }
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * UI
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    private void showDialog(final Activity activity, ItemClickEvent event) {
        final ImagesEntity entity = event.getImageEntity();

        if (mCustomDialog == null) {
            mCustomDialog = new CustomDialog.Builder(activity, false)
                    .setPositiveButton(getString(android.R.string.ok), Color.WHITE, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mCustomDialog.dismiss();
                        }
                    }).setNegativeButtonVisibility(View.GONE)
                    .prepare();

        }
        mCustomDialog.setTitle(entity.getTitle());

        Picasso.with(activity).load(entity.getUrl()).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                mCustomDialog.setIcon(new BitmapDrawable(getResources(), bitmap));
                mCustomDialog.show();
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                mCustomDialog.setIcon(R.mipmap.ic_launcher);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
    }
}
