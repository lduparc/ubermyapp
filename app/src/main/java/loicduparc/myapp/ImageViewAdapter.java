package loicduparc.myapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import loicduparc.myapp.models.ImagesEntity;
import loicduparc.myapp.utils.IItemClick;
import loicduparc.myapp.utils.ItemClickEvent;
import loicduparc.myapp.utils.LoadMoreEvent;

public class ImageViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements IItemClick {

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_EMPTY = 1;
    private static final int TYPE_LOAD_DATA = 2;
    private static final int MIN_ITEMS_COUNT = 1;

    private final Context mContext;
    private final List<ImagesEntity> mItems;

    public ImageViewAdapter(Context context, List<ImagesEntity> dataSet) {
        this.mContext = context;

        this.mItems = dataSet;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (TYPE_EMPTY == viewType) {
            final View view = LayoutInflater.from(mContext).inflate(R.layout.layout_empty, parent, false);
            return new EmptyViewHolder(view);
        } else if (TYPE_LOAD_DATA == viewType) {
            final View view = LayoutInflater.from(mContext).inflate(R.layout.layout_load_more, parent, false);
            return new LoadModeViewHolder(view);
        } else if (TYPE_ITEM == viewType) {
            final View view = LayoutInflater.from(mContext).inflate(R.layout.layout_item, parent, false);
            return new ItemViewHolder(view, this);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        if (TYPE_ITEM == viewType) {
            bindItemView((ItemViewHolder) holder, position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (this.mItems.size() == 0) {
            return TYPE_EMPTY;
        } else if (position == this.mItems.size()) {
            return TYPE_LOAD_DATA;
        }

        return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        int count = this.mItems.size();
        return count > 0 ? count + 1 : MIN_ITEMS_COUNT;
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * View Binder
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    private void bindItemView(final ItemViewHolder holder, int position) {
        ImagesEntity entity = this.mItems.get(position);
        holder.name.setText(entity.getTitle());

        Picasso.with(this.mContext)
                .load(entity.getTbUrl())
                .error(R.mipmap.ic_launcher)
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.thumb);
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * IItemClickListener
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    @Override
    public void showItemAtPosition(View view, int position) {
        if (position < this.mItems.size()) {
            EventBus.getDefault().post(new ItemClickEvent(view, this.mItems.get(position)));
        }
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * View Holder
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    static class EmptyViewHolder extends RecyclerView.ViewHolder {

        public EmptyViewHolder(View view) {
            super(view);
        }
    }

    static class LoadModeViewHolder extends RecyclerView.ViewHolder {

        public LoadModeViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }

        @OnClick(R.id.loadmore)
        void onItemClick() {
            EventBus.getDefault().post(new LoadMoreEvent());
        }
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {

        private IItemClick listener;

        @InjectView(R.id.thumb)
        ImageView thumb;

        @InjectView(R.id.name)
        TextView name;

        public ItemViewHolder(View view, IItemClick listener) {
            super(view);
            ButterKnife.inject(this, view);
            this.listener = listener;
        }

        @OnClick(R.id.container)
        void onItemClick() {
            if (this.listener != null) {
                this.listener.showItemAtPosition(thumb, getAdapterPosition());
            }
        }
    }

}
