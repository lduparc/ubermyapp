package loicduparc.myapp.communication;

import loicduparc.myapp.models.ResponseData;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

interface RestService {

    @GET("/images")
    void search(
            @Query("v") String version,
            @Query("q") String query,
            @Query("start") int page,
            Callback<ResponseData> callback
    );

}
