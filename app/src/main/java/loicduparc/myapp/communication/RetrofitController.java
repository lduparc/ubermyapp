package loicduparc.myapp.communication;

import loicduparc.myapp.models.ResponseData;
import loicduparc.myapp.models.SearchEvent;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;

public class RetrofitController {

    private static final String SERVICE_URL = "https://ajax.googleapis.com/ajax/services/search";
    private static final String SERVICE_URL_VERSION = "1.0";

    private static RetrofitController sInstance;

    private RestService mRestService;

    public static RetrofitController getInstance() {
        if (sInstance == null) {
            sInstance = new RetrofitController();
        }
        return sInstance;
    }

    private RetrofitController() {
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(SERVICE_URL).build();
        mRestService = adapter.create(RestService.class);
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Service
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    public void search(SearchEvent data, Callback<ResponseData> callback) {
        if (mRestService != null) {
            int page = data.getPage() + 1;
            String query = data.getConstraint();
            mRestService.search(SERVICE_URL_VERSION, query, page, callback);
        } else
            callback.failure(RetrofitError.unexpectedError("search", new Exception("RestService is null")));
    }
}
