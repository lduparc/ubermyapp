package loicduparc.myapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class ImagesEntity implements Parcelable {

    private String GsearchResultClass;
    private String width;
    private String height;
    private String imageId;
    private String tbWidth;
    private String tbHeight;
    private String unescapedUrl;
    private String url;
    private String visibleUrl;
    private String title;
    private String titleNoFormatting;
    private String originalContextUrl;
    private String content;
    private String contentNoFormatting;
    private String tbUrl;

    public ImagesEntity(Parcel source) {
        this.GsearchResultClass = source.readString();
        this.width = source.readString();
        this.height = source.readString();
        this.imageId = source.readString();
        this.tbWidth = source.readString();
        this.tbHeight = source.readString();
        this.unescapedUrl = source.readString();
        this.url = source.readString();
        this.visibleUrl = source.readString();
        this.title = source.readString();
        this.titleNoFormatting = source.readString();
        this.originalContextUrl = source.readString();
        this.content = source.readString();
        this.contentNoFormatting = source.readString();
        this.tbUrl = source.readString();
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Getters / Setters
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    public String getGsearchResultClass() {
        return GsearchResultClass;
    }

    public void setGsearchResultClass(String gsearchResultClass) {
        GsearchResultClass = gsearchResultClass;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getTbWidth() {
        return tbWidth;
    }

    public void setTbWidth(String tbWidth) {
        this.tbWidth = tbWidth;
    }

    public String getTbHeight() {
        return tbHeight;
    }

    public void setTbHeight(String tbHeight) {
        this.tbHeight = tbHeight;
    }

    public String getUnescapedUrl() {
        return unescapedUrl;
    }

    public void setUnescapedUrl(String unescapedUrl) {
        this.unescapedUrl = unescapedUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getVisibleUrl() {
        return visibleUrl;
    }

    public void setVisibleUrl(String visibleUrl) {
        this.visibleUrl = visibleUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleNoFormatting() {
        return titleNoFormatting;
    }

    public void setTitleNoFormatting(String titleNoFormatting) {
        this.titleNoFormatting = titleNoFormatting;
    }

    public String getOriginalContextUrl() {
        return originalContextUrl;
    }

    public void setOriginalContextUrl(String originalContextUrl) {
        this.originalContextUrl = originalContextUrl;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentNoFormatting() {
        return contentNoFormatting;
    }

    public void setContentNoFormatting(String contentNoFormatting) {
        this.contentNoFormatting = contentNoFormatting;
    }

    public String getTbUrl() {
        return tbUrl;
    }

    public void setTbUrl(String tbUrl) {
        this.tbUrl = tbUrl;
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * PARCEL
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    public static final Creator<ImagesEntity> CREATOR = new Creator<ImagesEntity>() {
        @Override
        public ImagesEntity createFromParcel(final Parcel source) {
            return new ImagesEntity(source);
        }

        @Override
        public ImagesEntity[] newArray(final int size) {
            return new ImagesEntity[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeString(GsearchResultClass);
        dest.writeString(width);
        dest.writeString(height);
        dest.writeString(imageId);
        dest.writeString(tbWidth);
        dest.writeString(tbHeight);
        dest.writeString(unescapedUrl);
        dest.writeString(url);
        dest.writeString(visibleUrl);
        dest.writeString(title);
        dest.writeString(titleNoFormatting);
        dest.writeString(originalContextUrl);
        dest.writeString(content);
        dest.writeString(contentNoFormatting);
        dest.writeString(tbUrl);
    }
}
