package loicduparc.myapp.models;

import java.util.List;

public class ImageResponseData {

    private List<ImagesEntity> results;
    private ImageCursor cursor;

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Getters / Setters
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    public List<ImagesEntity> getResults() {
        return results;
    }

    public void setResults(List<ImagesEntity> results) {
        this.results = results;
    }
}
