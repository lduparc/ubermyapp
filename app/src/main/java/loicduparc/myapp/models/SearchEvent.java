package loicduparc.myapp.models;

/**
 * Created by loicduparc on 28/04/15.
 */
public class SearchEvent {

    private String constraint;
    private boolean isNewQuery;
    private int page;

    public SearchEvent(String constraint, boolean newQuery, int page) {
        this.constraint = constraint;
        this.isNewQuery = newQuery;
        this.page = page;
    }

    public SearchEvent(String constraint, boolean newQuery) {
        this(constraint, newQuery, 0);
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Getters / Setters
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    public String getConstraint() {
        return constraint;
    }

    public boolean isNewQuery() {
        return isNewQuery;
    }

    public void setIsNewQuery(boolean isNewQuery) {
        this.isNewQuery = isNewQuery;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
