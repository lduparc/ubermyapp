package loicduparc.myapp.utils;

import android.view.View;

/**
 * Created by loicduparc on 29/04/15.
 */
public interface IItemClick {

    void showItemAtPosition(View view, int position);
}
