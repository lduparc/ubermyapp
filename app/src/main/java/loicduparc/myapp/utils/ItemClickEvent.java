package loicduparc.myapp.utils;

import android.view.View;

import loicduparc.myapp.models.ImagesEntity;

public class ItemClickEvent {

    private View view;
    private ImagesEntity entity;

    public ItemClickEvent(View view, ImagesEntity entity) {
        this.view = view;
        this.entity = entity;
    }

    public View getView() {
        return view;
    }

    public ImagesEntity getImageEntity() {
        return entity;
    }
}
