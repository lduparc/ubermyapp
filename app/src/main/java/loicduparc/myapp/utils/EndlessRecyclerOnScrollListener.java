package loicduparc.myapp.utils;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
 
public abstract class EndlessRecyclerOnScrollListener extends RecyclerView.OnScrollListener {
    public static String TAG = EndlessRecyclerOnScrollListener.class.getSimpleName();
 
    private int mPreviousTotal = 0;
    private boolean mLoading = true;
    private int mVisibleThreshold = 4;
    private int mFirstVisibleItem;
    private int mVisibleItemCount;
    private int mTotalItemCount;
    private int mCurrent_page = 1;
 
    private LinearLayoutManager mLinearLayoutManager;
 
    public EndlessRecyclerOnScrollListener(LinearLayoutManager linearLayoutManager) {
        this.mLinearLayoutManager = linearLayoutManager;
    }
 
    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
 
        this.mVisibleItemCount = recyclerView.getChildCount();
        this.mTotalItemCount = mLinearLayoutManager.getItemCount();
        this.mFirstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();

        if (mLoading) {
            if (mTotalItemCount > mPreviousTotal) {
                mLoading = false;
                mPreviousTotal = mTotalItemCount;
            }
        }
        if (!mLoading && (mTotalItemCount - mVisibleItemCount) <= (mFirstVisibleItem + mVisibleThreshold)) {

            mCurrent_page++;
            onLoadMore(mCurrent_page);
            mLoading = true;
        }
    }
 
    public abstract void onLoadMore(int current_page);
}